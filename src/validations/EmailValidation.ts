import { EmailValidationModel } from "@/models/login/EmailValidationModel";
import { AlertMessage } from "@/models/message/AlertMessage";

export class EmailValidation
{
    static validate(email: string): EmailValidationModel
    {
        // eslint-disable-next-line
        const regex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        const model = new EmailValidationModel;
        model.isValid = regex.test(email);
        model.alert = new AlertMessage;
        
        if(model.isValid){
            model.alert.message ='Entered email is valid!';
            model.alert.variant = "success";
        }else{
            model.alert.message ='Please enter valid email adress!';
            model.alert.variant = "danger";
        }
      
        model.alert.show = true;
        return model; 
    }
}