import { SelectModel } from "@/models/general/SelectModel";
import { LoginValidationModel } from "@/models/login/LoginValidationModel";
import { RegisterEmail } from "@/models/login/RegisterEmail";
import { AlertMessage } from "@/models/message/AlertMessage";

export abstract class LoginValidation{
    
    static validate(registerEmail: RegisterEmail, registrationOptions: Array<SelectModel>): LoginValidationModel{
        let isValid= true;
        const loginValidationModel = new LoginValidationModel;

        if(registrationOptions[registerEmail.selectedOption.value].text == 'SMS')
        {
            loginValidationModel.alert = new AlertMessage;
            loginValidationModel.alert.message ='Chosen signin method is not supported yet!';
            loginValidationModel.alert.variant = "danger";
            loginValidationModel.alert.show = true;
            isValid=false;
        }

        if(registrationOptions[registerEmail.selectedOption.value].value == 0){
            loginValidationModel.alert = new AlertMessage;
            loginValidationModel.alert.message ='Please chose sign in method!';
            loginValidationModel.alert.variant = "warning";
            loginValidationModel.alert.show = true;
            isValid=false;
        }

        loginValidationModel.isValid = isValid;

        return loginValidationModel;

    }
}