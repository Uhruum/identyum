import { AlertMessage } from "@/models/message/AlertMessage";
import { PinValidationModel } from "@/models/pin/PinValidationModel";

export class PinValidation
{
    static validate(pin: string, field: string): PinValidationModel
    {
        const regex = new RegExp('^[0-9]{6}$');
        const model = new PinValidationModel;
        model.isValid = regex.test(pin);
        model.alert = new AlertMessage;
        
        if(model.isValid){
            model.alert.message ='Entered '+field+' is valid!';
            model.alert.variant = "success";
        }else{
            model.alert.message ='Entered '+field+' is not valid!';
            model.alert.variant = "danger";
        }
      
        model.alert.show = true;
        return model; 
    }
}