import { FormValidationModel } from "@/models/form/FormValidationModel";
import { AlertMessage } from "@/models/message/AlertMessage";

export class FormValidation
{
    static validate(value: string, field: string, pattern: string): FormValidationModel
    {
        const regex = new RegExp(pattern);
        const model = new FormValidationModel;
        model.isValid = regex.test(value);
        model.alert = new AlertMessage;
        
        if(model.isValid){
            model.alert.message ='Entered '+field+' is valid!';
            model.alert.variant = "success";
        }else{
            model.alert.message ='Entered '+field+' is not valid!';
            model.alert.variant = "danger";
        }
      
        model.alert.show = true;
        return model; 
    }

    static validateRequired(value: string, field: string): FormValidationModel
    {
        const model = new FormValidationModel;
        if(typeof(value) === 'undefined' || value == null ||  value ===''){
            model.isValid = false;
        }else
            model.isValid = true;
        
        model.alert = new AlertMessage;
        
        if(model.isValid){
            model.alert.message ='';
            model.alert.variant = "success";
        }else{
            model.alert.message =''+field+' is required!';
            model.alert.variant = "danger";
        }
      
        model.alert.show = true;
        return model; 
    }
}