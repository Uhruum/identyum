import axios, { AxiosError } from 'axios'
import { AuthResponse } from '../models/authentication/AuthResponse';
import { AppToken } from '@/models/authentication/AppToken';
import { AuthRequest } from '@/models/authentication/AuthRequest';
import { ErrorModelProvider } from '@/errorHandling/ErrorModelProvider';

export abstract class AuthenticationService {
    private static authAxios = axios.create();

    static async authenticate(): Promise<AuthResponse>{

        try 
        {
            const res = new AuthResponse;
            const data = new AuthRequest("interview_013","#interview_013$");
            const axiosResponse = await this.authAxios.post<AppToken>(process.env.VUE_APP_AUTHURL,data);
            res.token = axiosResponse.data;
            return res;
        } 
        catch (error) {
            const res = new AuthResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            return res;
        }
    }
}