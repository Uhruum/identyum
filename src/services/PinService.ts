import { ErrorModelProvider } from "@/errorHandling/ErrorModelProvider";
import { ErrorModel } from "@/models/ErrorModel";
import { PinDefaultResponse } from "@/models/pin/PinDefaultResponse";
import { PinSetRequest } from "@/models/pin/PinSetRequest";
import { PinStartModel } from "@/models/pin/PinStartModel";
import { PinStartResponse } from "@/models/pin/PinStartResponse";
import { PinSubmitRequest } from "@/models/pin/PinSubmitRequest";
import axios, { AxiosError } from "axios";

export abstract class PinService {
    private static authAxios = axios.create();

    static async start(accessToken: string): Promise<PinStartResponse>{

        try 
        {
            const res = new PinStartResponse;
            this.authAxios.defaults.headers.common['Authorization'] = 'Bearer '+ accessToken;
            const axiosResponse = await this.authAxios.get<PinStartModel>(process.env.VUE_APP_PINSTARTURL);
            res.data = axiosResponse.data;
            res.success=true;
            return res;
        } 
        catch (error) {
            const res = new PinStartResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            res.success = false;
            return res;
        }
    }

    static async set(pinSetRequest: PinSetRequest): Promise<PinDefaultResponse>{

        try 
        {
            this.authAxios.defaults.headers.common['Authorization'] = 'Bearer '+ pinSetRequest.accessToken;
            await this.authAxios.post(process.env.VUE_APP_PINSETURL,pinSetRequest.data);
            const res = new PinDefaultResponse;
            res.success=true;
            return res;
        } 
        catch (error) {
            const res = new PinDefaultResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            res.success = false;
            return res;
        }
    }

    static async submit(pinSubmitRequest: PinSubmitRequest): Promise<PinDefaultResponse>{

        try 
        {
            this.authAxios.defaults.headers.common['Authorization'] = 'Bearer '+ pinSubmitRequest.accessToken;
            await this.authAxios.post(process.env.VUE_APP_PINSUBMITURL,{"pin": pinSubmitRequest.pin});
            const res = new PinDefaultResponse;
            res.success=true;
            return res;
        } 
        catch (error) {
            const res = new PinDefaultResponse;
            const err = error as AxiosError;
            if(err.response)
            {
                if(err.response.status == 422)
                {
                    res.error = new ErrorModel;
                    res.error.status = err.response.status;
                    res.error.message = err.response.data.message;
                    res.success = false;
                    return res;
                }
                if(err.response.status == 418)
                {
                    res.error = new ErrorModel;
                    res.error.status = err.response.status;
                    res.error.message = err.response.data.message;
                    res.success = false;
                    return res;
                }
            }

            res.error = ErrorModelProvider.provide(err);
            res.success = false;
            return res;
        }
    }
}