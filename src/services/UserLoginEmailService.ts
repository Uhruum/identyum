import { EmailResponse } from '@/models/login/EmailResponse';
import { RegistrationOptions } from '@/models/login/RegistrationOptions';
import { SelectEmailResponse } from '@/models/login/SelectEmailResponse';
import { StartResponse } from '@/models/login/StartResponse';
import { SubmitEmailRequest } from '@/models/login/SubmitEmailRequest';
import { SubmitEmailResponse } from '@/models/login/SubmitEmailResponse';
import { VerifyEmailRequest } from '@/models/emailVerification/VerifyEmailRequest';
import { VerifyEmailResponse } from '@/models/emailVerification/VerifyEmailResponse';
import axios, { AxiosError } from 'axios'
import { AccessToken } from '@/models/authentication/AccessToken';
import { ErrorModelProvider } from '@/errorHandling/ErrorModelProvider';
export abstract class UserLoginEmailService {
    private static authAxios = axios.create();

    static async start(sessionState: string): Promise<StartResponse>{

        try 
        {
            const res = new StartResponse;
            this.authAxios.defaults.headers.common['X-SESSION-UUID'] = sessionState;
            const axiosResponse = await this.authAxios.get<RegistrationOptions>(process.env.VUE_APP_LOGINSTARTURL);
            res.options = axiosResponse.data;
            return res;
        } 
        catch (error) {
            const res = new StartResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            return res;
        }
    }

    static async selectEmail(sessionState: string): Promise<SelectEmailResponse>{

        try 
        {
            const res = new SelectEmailResponse;
            this.authAxios.defaults.headers.common['X-SESSION-UUID'] = sessionState;
            const axiosResponse = await this.authAxios.get<EmailResponse>(process.env.VUE_APP_LOGINSELECTEMAIL);
            res.response = axiosResponse.data;
            return res;
        } 
        catch (error) {
            const res = new SelectEmailResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            return res;
        }
    }

    static async submitEmail(data: SubmitEmailRequest): Promise<SubmitEmailResponse>{

        try 
        {
            const res = new SubmitEmailResponse;
            this.authAxios.defaults.headers.common['X-SESSION-UUID'] = data.sessionState;
            await this.authAxios.post(process.env.VUE_APP_LOGINSUBMITEMAILURL,data.data);
            res.success= true;
            return res;
        } 
        catch (error) {
            const res = new SubmitEmailResponse;
            res.success= false;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            return res;
        }
    }

    static async verifyEmail(data: VerifyEmailRequest): Promise<VerifyEmailResponse>{

        try 
        {
            const res = new VerifyEmailResponse;
            this.authAxios.defaults.headers.common['X-SESSION-UUID'] = data.sessionState;
            const axiosResponse  = await this.authAxios.post<AccessToken>(process.env.VUE_APP_LOGINEMAILVERIFYURL,data.otpModel);
            res.appToken= axiosResponse.data.accessToken; 
            res.success= true;
            return res;
        } 
        catch (error) {
            const res = new VerifyEmailResponse;
            res.success= false;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            return res;
        }
    }
}