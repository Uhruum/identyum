import { ErrorModelProvider } from "@/errorHandling/ErrorModelProvider";
import { FormDefaultResponse } from "@/models/form/FormDefaultResponse";
import { FormStartResponse } from "@/models/form/FormStartResponse";
import { StartResponse } from "@/models/form/StartResponse";
import { SubmitEntryRequest } from "@/models/form/SubmitEntryRequest";
import axios, { AxiosError } from "axios";

export abstract class FormService{
    private static authAxios = axios.create();

    static async start(accessToken: string): Promise<FormStartResponse>{

        try 
        {
            const res = new FormStartResponse;
            this.authAxios.defaults.headers.common['Authorization'] = 'Bearer '+ accessToken;
            const axiosResponse = await this.authAxios.get<StartResponse>(process.env.VUE_APP_FORMSTARTURL);
            res.data = axiosResponse.data;
            res.success=true;
            return res;
        } 
        catch (error) {
            const res = new FormStartResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            res.success = false;
            return res;
        }
    }

    static async submit(accessToken: string, data : SubmitEntryRequest): Promise<FormDefaultResponse>{

        try 
        {
            const res = new FormDefaultResponse;
            this.authAxios.defaults.headers.common['Authorization'] = 'Bearer '+ accessToken;
            await this.authAxios.post(process.env.VUE_APP_FORMSUBMITURL,data);
            res.success=true;
            return res;
        } 
        catch (error) {
            const res = new FormDefaultResponse;
            res.error = ErrorModelProvider.provide(error as AxiosError);
            res.success = false;
            return res;
        }
    }
}