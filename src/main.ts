import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Token from './components/Token.vue';
import Pin from './components/Pin.vue';
import IdentyumGenericForm from './components/IdentyumGenericForm.vue';

Vue.component('home', Home);
Vue.component('login', Login);
Vue.component('token', Token);
Vue.component('pin', Pin);
Vue.component('identyum-generic-form', IdentyumGenericForm);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
