import { ErrorMessagesProvider } from "./ErrorMessagesProvider";

export abstract class ErrorMessageResolver
{
    static resolve(status: number): string
    {
        return ErrorMessagesProvider.provideMessages().get(status) as string;
    }
}