export abstract class ErrorMessagesProvider  {
    
    static errorDictionary = new Map<number, string>([
        [400, "Error occured please contact administrator!"],
        [401, "Authorization failed! Try again later!"],
        [403, "Forbidden! You dont have rights to access this!"],
        [405, "Login already done!"],
        [412, "Precondition Failed - EMAIL_NOT_VERIFIED, PHONE_NOT_VERIFIED"],
        [418, "Account blocked!"],
        [422, "Wrong PIN!"],
        [429, "Too many OTPs requested! Try again later!"],
        [500, "Error occured please contact administrator!"]
    ]);
  
    static provideMessages() : Map<number, string> {
       return this.errorDictionary
    }
}