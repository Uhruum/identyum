import { SubmitEmailModel } from "./SubmitEmailModel";

export class SubmitEmailRequest
{
    sessionState!: string;
    data!: SubmitEmailModel;
}