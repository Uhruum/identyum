export class EmailResponse {
    errorUuid!: string;
    prefilledEmail!: string;
    submitTimeout!: number;
  }