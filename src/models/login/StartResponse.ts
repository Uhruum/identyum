import { ErrorModel } from "../ErrorModel";
import { RegistrationOptions } from "./RegistrationOptions";

export class StartResponse {
    options!: RegistrationOptions;
    error!: ErrorModel;
  }