import { ErrorModel } from "../ErrorModel";
import { EmailResponse } from "./EmailResponse";

export class SelectEmailResponse
{
    response!: EmailResponse;
    error!: ErrorModel;
}