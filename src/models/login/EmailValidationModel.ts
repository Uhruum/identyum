import { IValidationModel } from "@/interfaces/IValidationModel";
import { AlertMessage } from "../message/AlertMessage";

export class EmailValidationModel implements IValidationModel{
    alert!: AlertMessage;
    isValid!: boolean;
}