import { ErrorModel } from "../ErrorModel";

export class SubmitEmailResponse
{
    success!: boolean;
    error!: ErrorModel;
}