import { AlertMessage } from "../message/AlertMessage";
import { IValidationModel } from "@/interfaces/IValidationModel";
export class LoginValidationModel implements IValidationModel
{
    alert!: AlertMessage;
    isValid!: boolean;
}