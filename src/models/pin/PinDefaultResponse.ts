import { ErrorModel } from "../ErrorModel";

export class PinDefaultResponse
{
    success!: boolean;
    error!: ErrorModel;
}