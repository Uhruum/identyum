import { PinSetModel } from "./PinSetModel";

export class PinSetRequest{
    data!: PinSetModel;
    accessToken!: string;
}