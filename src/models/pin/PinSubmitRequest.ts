export class PinSubmitRequest
{
    accessToken!: string;
    pin!: string;
}