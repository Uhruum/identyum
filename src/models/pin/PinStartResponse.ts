import { ErrorModel } from "../ErrorModel";
import { PinStartModel } from "./PinStartModel";

export class PinStartResponse
{
    success!: boolean;
    error!: ErrorModel;
    data!: PinStartModel;
}