import { IValidationModel } from "@/interfaces/IValidationModel";
import { AlertMessage } from "../message/AlertMessage";

export class PinValidationModel implements IValidationModel{
    alert!: AlertMessage;
    isValid!: boolean;
    
}