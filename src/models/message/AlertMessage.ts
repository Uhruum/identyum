export class AlertMessage{
    message: string;
    variant: string;
    show: boolean;

    constructor(){
        this.message = "";
        this.variant = "success";
        this.show= false;
    }
}