import { Translation } from "./Translation";

export class Option{
    selected!: boolean;
    text!: string;
    textTranslations!: Array<Translation>
    value!: string;
}