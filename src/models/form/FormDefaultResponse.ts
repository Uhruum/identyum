import { ErrorModel } from "../ErrorModel";

export class FormDefaultResponse
{
    success!: boolean;
    error!: ErrorModel;
}