import { Option } from "./Option";
import { Translation } from "./Translation";

export class Element
{
    label!: string;
    labelTranslations!: Array<Translation>;
    max!: number;
    maxLength!: number;
    min!: number;
    multiple!: boolean;
    name!: string;
    options!: Array<Option>;
    pattern!: string;
    prefilledValue!: string;
    required!: boolean;
    tag!: string;
    type!: string;
    validations!: Array<string>;
}