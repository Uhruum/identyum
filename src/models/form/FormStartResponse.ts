import { ErrorModel } from "../ErrorModel";
import { StartResponse } from "./StartResponse";

export class FormStartResponse{
    success!: boolean;
    data!: StartResponse;
    error!: ErrorModel;
}