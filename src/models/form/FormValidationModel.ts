import { IValidationModel } from "@/interfaces/IValidationModel";
import { AlertMessage } from "../message/AlertMessage";

export class FormValidationModel implements IValidationModel{
    alert!: AlertMessage;
    isValid!: boolean;
}