import { ErrorModel } from "../ErrorModel";
import { AppToken } from "./AppToken";

export class AuthResponse {
    token!: AppToken;
    error!: ErrorModel;
  }