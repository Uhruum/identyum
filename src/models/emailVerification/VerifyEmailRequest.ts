import { OtpModel } from "./OtpModel";

export class VerifyEmailRequest
{
    sessionState!: string;
    otpModel!: OtpModel;
}