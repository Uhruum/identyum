import { AppToken } from "../authentication/AppToken";
import { ErrorModel } from "../ErrorModel";

export class VerifyEmailResponse
{
    success!: boolean;
    appToken!: AppToken;
    error!: ErrorModel;
}