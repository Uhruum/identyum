export default{
    AppName: process.env.APP_NAME ?? '',
    BaseUrl: process.env.BASE_URI ?? '',
    AuthUrlSuffix: process.env.AUTH_URI_SUFFIX ?? ''
}