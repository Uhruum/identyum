import { AlertMessage } from "@/models/message/AlertMessage";

export interface IValidationModel{
    alert: AlertMessage;
    isValid: boolean;
}